/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class CriarAlunoController implements Initializable {

    @FXML
    private Label lblNovaCandidatura;
    @FXML
    private Label lblNovaCandidatura1;
    @FXML
    private Label lblCurso1;
    @FXML
    private ChoiceBox<?> comboBoxNomeCursos1;
    @FXML
    private Label lblNomeAluno1;
    @FXML
    private Label lblIdentificaçãoCivil1;
    @FXML
    private Button buttonVoltarAtras1;
    @FXML
    private MaterialDesignIconView iconVoltarAtras1;
    @FXML
    private Label lblContatoTelefónico1;
    @FXML
    private TextField txtContatoTelefónico1;
    @FXML
    private TextField txtMorada1;
    @FXML
    private Label lblMorada1;
    @FXML
    private TextField txtNomeAluno1;
    @FXML
    private TextField txtIdentificaçãoCivil1;
    @FXML
    private TextField txtEmail1;
    @FXML
    private Label lblEmail1;
    @FXML
    private Label lblCurso;
    @FXML
    private ChoiceBox<?> comboBoxNomeCursos;
    @FXML
    private Label lblNomeAluno;
    @FXML
    private Label lblIdentificaçãoCivil;
    @FXML
    private Button buttonVoltarAtras;
    @FXML
    private MaterialDesignIconView iconVoltarAtras;
    @FXML
    private Label lblContatoTelefónico;
    @FXML
    private TextField txtContatoTelefónico;
    @FXML
    private TextField txtMorada;
    @FXML
    private Label lblMorada;
    @FXML
    private TextField txtNomeAluno;
    @FXML
    private TextField txtIdentificaçãoCivil;
    @FXML
    private TextField txtEmail;
    @FXML
    private Label lblEmail;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void Displayoptions(MouseEvent event) {
    }

    @FXML
    private void voltarAtras(ActionEvent event) {
    }
    
}
