package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cláudia Nunes
 */
public class Aluno {
    private int numAluno;
    private String nomeCompleto;
    private int nif;
    private String rua;
    private String codPostal;
    private String contactoTelefonico;
    private String email;

    public Aluno(int numAluno, String nomeCompleto, int nif, String rua, String codPostal, String contactoTelefonico, String email) {
        this.numAluno = numAluno;
        this.nomeCompleto = nomeCompleto;
        this.nif = nif;
        this.rua = rua;
        this.codPostal = codPostal;
        this.contactoTelefonico = contactoTelefonico;
        this.email = email;
    }
    
    public int getNumAluno() {
        return numAluno;
    }

    public void setNumAluno(int numAluno) {
        this.numAluno = numAluno;
    }
    
    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public int getNif() {
        return nif;
    }

    public void setNif(int nif) {
        this.nif = nif;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getCodPostal() {
        return codPostal;
    }

    public void setCodPostal(String codPostal) {
        this.codPostal = codPostal;
    }

    public String getContactoTelefonico() {
        return contactoTelefonico;
    }

    public void setContactoTelefonico(String contactoTelefonico) {
        this.contactoTelefonico = contactoTelefonico;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

   }
