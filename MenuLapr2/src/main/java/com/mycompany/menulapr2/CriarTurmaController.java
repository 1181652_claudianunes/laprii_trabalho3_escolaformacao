/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.menulapr2;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * FXML Controller class
 *
 * @author Cláudia Nunes
 */
public class CriarTurmaController implements Initializable {

    @FXML
    private Label txtTitulo;
    @FXML
    private Label txtSelecionarCurso;
    @FXML
    private ComboBox<?> cbMostrarCursos;
    @FXML
    private Label txtNumeroTurmas;
    @FXML
    private Text txtMaxTurmas;
    @FXML
    private Label lblVagasCursos;
    @FXML
    private TextField txtfieldNumeroTurmas;
    @FXML
    private Label txtNumeroAlunosTurma;
    @FXML
    private Text txtMaxAlunosTurma;
    @FXML
    private Label lblNumeroAlunosInscritos;
    @FXML
    private TextField txtfieldNumeroAlunosTurma;
    @FXML
    private Text txtDuracaoTurmas;
    @FXML
    private DatePicker dateDataInicial;
    @FXML
    private Text txtAte;
    @FXML
    private DatePicker dateDataFinal;
    @FXML
    private Button buttonCriarTurmas;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
