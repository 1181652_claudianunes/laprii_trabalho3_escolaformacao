package com.mycompany.menulapr2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;

/**
 * FXML Controller class
 *
 * @author Goldb
 */
public class ListarTurmasController implements Initializable {

    @FXML
    private TableView<Turma> tblViewTurma;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        preencherView();
    }    
    
    
    public void preencherView(){
        ObservableList<Turma> listaTurmas = FXCollections.observableArrayList();
        try{
            
            Statement query = Conexao.connectarBD().createStatement();
            ResultSet resultados = query.executeQuery("SELECT * FROM turmaCurso");
            while(resultados.next()){
                Turma turma = new Turma(resultados.getString("nome"),resultados.getString("data_inicio"),resultados.getString("data_fim"),resultados.getString("nomeTurma"));
                listaTurmas.add(turma);
            }
                    
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        
        tblViewTurma.setItems(listaTurmas);
    }
    
}
