package com.mycompany.menulapr2;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AdministracaoController implements Initializable {
    
    @FXML
    private Button buttonNovoCurso;
    @FXML
    private Button buttonListarCursos;
    @FXML
    private Button buttonNovaCandidatura;
    @FXML
    private Button buttonListarAlunos;
    @FXML
    private Button buttonNovaTurma;
    @FXML
    private Label label1;
    @FXML
    private Label label11;
    @FXML
    private Label label111;
    @FXML
    private Label label1111;
    @FXML
    private Label label11111;
    @FXML
    private Label label111111;
    @FXML
    private Label label1111111;
    @FXML
    private Label label1111112;
    @FXML
    private Button buttonListarTurmas;
    @FXML
    private Button buttonNovaDisciplina;
    @FXML
    private Button buttonListarDisciplinas;
    @FXML
    private Button buttonNovoFormador;
    @FXML
    private Button buttonListarFormadores;
    @FXML
    private Button buttonNovoHorario;
    @FXML
    private Button buttonListarHorarios;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void novoCurso(ActionEvent e) throws IOException{
        
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/NovoCurso.fxml"));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();
        
        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2); 
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }
    
    @FXML
    public void listarCurso(ActionEvent e) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarCurso.fxml"));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();  
        
        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2); 
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }
    
    @FXML
    public void listarAlunos(ActionEvent e) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarAlunos.fxml"));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show(); 
        
        javafx.geometry.Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        stage.setX((primScreenBounds.getWidth() - stage.getWidth()) / 2); 
        stage.setY((primScreenBounds.getHeight() - stage.getHeight()) / 4);
    }
    
    @FXML
    public void listarTurmas(ActionEvent e) throws IOException{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/ListarTurmas.fxml"));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");
        
        Stage stage = (Stage) ((Node) e.getSource()).getScene().getWindow();
        stage.setTitle("Gestão de Escola de Formação");
        stage.setScene(scene);
        stage.show();  
    }
}
